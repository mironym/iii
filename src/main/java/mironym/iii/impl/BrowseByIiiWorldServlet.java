package mironym.iii.impl;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URI;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;

/**
 * The servlet realizing the ability to browse an issue directly by using the
 * internal id.\
 * 
 * Example: <your jira>/jira/plugins/servlet/iii?id=48662
 * 
 * @author mironym
 *
 */
public class BrowseByIiiWorldServlet extends HttpServlet {
	private static final long serialVersionUID = 7179092328082219674L;

	/**
	 * All the logic is here: 0. checks if the user is logged in, if not go through login redirect; 1. get the id
	 * from the request parameter; if null, report an error; 2. look up the
	 * corresponding Issue, if not found, report an error; 3. redirect to
	 * /browse/<key>
	 * 
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest,
	 *      javax.servlet.http.HttpServletResponse)
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 0.
		if (!ComponentAccessor.getJiraAuthenticationContext().isLoggedInUser()) {
			redirect(request, response, "/login.jsp?permissionViolation=true&os_destination=" + getUri(request));
			
//			HttpServletRequest request = getHttpRequest();
//		    String returnURL = getCurrentURL();
//		    if (request.getQueryString() != null)
//		    {
//		        returnURL += "?" + request.getQueryString();
//		    }
//		    return getRedirect("/login.jsp?os_destination=" + JiraUrlCodec.encode(returnURL), false);

			return;
		}

		// 1.
		String idParameter = request.getParameter("id");
		if (idParameter == null) {
			reportError("No id parameter (?id=) specified.", response);
			return; // from the method
		}

		Long id;
		try {
			id = new Long(idParameter.trim());
		} catch (NumberFormatException nfe) {
			reportError("Invalid id specified.", response);
			return; // from the method
		}

		// 2.
		Issue issue = ComponentAccessor.getIssueManager().getIssueObject(id);

		if (issue == null) {
			reportError("Issue with the specified id not found.", response);
			return; // from the method
		}

		// 3.
		redirect(request, response, "/browse/" + issue.getKey());
	}

	/**
	 * redirects the servlet to the request.getContextPath() + urlSuffix
	 * 
	 * @param request
	 * @param response
	 * @param urlSuffix
	 *            the url suffix staring with /
	 * @throws IOException
	 */
	private void redirect(HttpServletRequest request, HttpServletResponse response, String urlSuffix)
			throws IOException {
		response.sendRedirect(request.getContextPath() + urlSuffix);
	}

	private URI getUri(HttpServletRequest request) {
		StringBuffer builder = request.getRequestURL();
		if (request.getQueryString() != null) {
			builder.append("?");
			builder.append(request.getQueryString());
		}
		return URI.create(builder.toString());
	}

	/**
	 * Aux method for generating a html error page with the given message.
	 */
	private void reportError(String message, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");

		PrintWriter out = response.getWriter();
		out.println("<h1>Internal Issue ID Servlet Error</h1><p>" + message + "</p>");
		out.flush();
	}
}
