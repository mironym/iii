package mironym.iii.impl;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.impl.GenericTextCFType;
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.TextFieldCharacterLengthValidator;
import com.atlassian.jira.security.JiraAuthenticationContext;

public class InternalIssueId extends GenericTextCFType {
	public InternalIssueId() {
		super(getComponent(CustomFieldValuePersister.class), getComponent(GenericConfigManager.class),
				getComponent(TextFieldCharacterLengthValidator.class), getComponent(JiraAuthenticationContext.class));
	}

	private static <T> T getComponent(Class<T> tClass) {
		return ComponentAccessor.getComponent(tClass);
	}

	@Override
	public String getValueFromIssue(CustomField field, Issue issue) {
		return issue.getId().toString();
	}

}